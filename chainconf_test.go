/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package chainconf

import (
	"fmt"
	"testing"

	"chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/pb-go/v2/config"
	"chainmaker.org/chainmaker/pb-go/v2/syscontract"
	"chainmaker.org/chainmaker/protocol/v2/mock"
	"chainmaker.org/chainmaker/protocol/v2/test"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestGenesis(t *testing.T) {
	log = &test.GoLogger{}
	genesis, err := Genesis("./testdata/bc1.yml")
	require.Nil(t, err)
	fmt.Println(genesis)
}

func TestChainConf_GetChainConfigFromFuture(t *testing.T) {
	ctl := gomock.NewController(t)
	bs := mock.NewMockBlockchainStore(ctl)
	//block122 := &common.Block{Header: &common.BlockHeader{BlockHeight: 122, PreConfHeight: 0}}
	block0 := &common.Block{Header: &common.BlockHeader{BlockHeight: 0, PreConfHeight: 0}}
	chainConfig := &config.ChainConfig{ChainId: "chain1", Version: "v2"}
	result, _ := chainConfig.Marshal()
	configtx := &common.Transaction{
		Payload: &common.Payload{
			ChainId:        "chain1",
			TxType:         0,
			TxId:           "gengesis",
			Timestamp:      0,
			ExpirationTime: 0,
			ContractName:   syscontract.SystemContract_CHAIN_CONFIG.String(),
			Method:         syscontract.SystemContract_CHAIN_CONFIG.String(),
			Parameters:     nil,
			Sequence:       0,
			Limit:          nil,
		},
		Result: &common.Result{
			Code: 0,
			ContractResult: &common.ContractResult{
				Code:          0,
				Result:        result,
				Message:       "ok",
				GasUsed:       0,
				ContractEvent: nil,
			},
			RwSetHash: nil,
			Message:   "OK",
		},
	}
	block0.Txs = []*common.Transaction{configtx}
	//bs.EXPECT().GetBlock(uint64(122)).Return(block122, nil)
	bs.EXPECT().GetBlock(uint64(0)).Return(block0, nil)
	bs.EXPECT().GetBlockHeaderByHeight(uint64(122)).Return(&common.BlockHeader{BlockType: common.BlockType_NORMAL_BLOCK, PreConfHeight: 0}, nil)
	defer ctl.Finish()
	cc, err := NewChainConf(WithBlockchainStore(bs))
	assert.Nil(t, err)
	cconfig, err := cc.GetChainConfigFromFuture(123)
	assert.Nil(t, err)
	t.Log(cconfig)
}
